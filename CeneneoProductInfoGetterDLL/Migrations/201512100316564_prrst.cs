namespace CeneneoProductInfoGetterDLL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class prrst : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Opinions", "ProductRecommended", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Opinions", "ProductRecommended", c => c.Boolean(nullable: false));
        }
    }
}
