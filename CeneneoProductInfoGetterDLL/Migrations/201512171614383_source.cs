namespace CeneneoProductInfoGetterDLL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class source : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Opinions", "src", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Opinions", "src");
        }
    }
}
