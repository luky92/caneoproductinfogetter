namespace CeneneoProductInfoGetterDLL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class final : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Opinions", "ProsList", c => c.String());
            AddColumn("dbo.Opinions", "ConsList", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Opinions", "ConsList");
            DropColumn("dbo.Opinions", "ProsList");
        }
    }
}
