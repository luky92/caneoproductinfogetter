namespace CeneneoProductInfoGetterDLL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class m2 : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Opinions", name: "CeneoProduct_Id", newName: "Product_Id");
            RenameIndex(table: "dbo.Opinions", name: "IX_CeneoProduct_Id", newName: "IX_Product_Id");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Opinions", name: "IX_Product_Id", newName: "IX_CeneoProduct_Id");
            RenameColumn(table: "dbo.Opinions", name: "Product_Id", newName: "CeneoProduct_Id");
        }
    }
}
