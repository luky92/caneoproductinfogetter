namespace CeneneoProductInfoGetterDLL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Opinions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CeneoOpinionId = c.Int(nullable: false),
                        Author = c.String(),
                        OpinionSummary = c.String(),
                        StarsAvrage = c.Single(nullable: false),
                        OpinionDateTime = c.DateTime(nullable: false),
                        ProductRecommended = c.Boolean(nullable: false),
                        OpinionUsefullnesCount = c.Int(nullable: false),
                        CeneoProduct_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CeneoProducts", t => t.CeneoProduct_Id)
                .Index(t => t.CeneoProduct_Id);
            
            CreateTable(
                "dbo.CeneoProducts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CeneoProductId = c.Int(nullable: false),
                        Make = c.String(),
                        Model = c.String(),
                        ProductType = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Opinions", "CeneoProduct_Id", "dbo.CeneoProducts");
            DropIndex("dbo.Opinions", new[] { "CeneoProduct_Id" });
            DropTable("dbo.CeneoProducts");
            DropTable("dbo.Opinions");
        }
    }
}
