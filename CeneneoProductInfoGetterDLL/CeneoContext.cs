﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CeneneoProductInfoGetterDLL
{
    /// <summary>
    /// Klaca dla entity framework code first 
    /// </summary>
    public class CeneoContext : DbContext
    {
        public CeneoContext() : base("name=TRN_DB")
        {
            
        }

        public DbSet<CeneoProduct> Products { get; set; }
        public DbSet<Opinion> Opinions { get; set; }
    }
}
