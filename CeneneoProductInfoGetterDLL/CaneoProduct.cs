﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;

namespace CeneneoProductInfoGetterDLL
{/// <summary>
/// reprezentacja produktu w progaramie i bazie 
/// </summary>
    public partial class CeneoProduct
    {
        /// <summary>
        /// Konstruktor klasy
        /// </summary>
        public CeneoProduct() 
        {
            Opinions=new List<Opinion>();
        }
        [Key]
        
        public int Id { get; set; }
        public int CeneoProductId { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string ProductType { get; set; }
        public virtual ICollection<Opinion> Opinions { get; set; }
    }
}
