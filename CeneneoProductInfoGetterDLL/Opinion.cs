﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CeneneoProductInfoGetterDLL
{
    /// <summary>
    /// Klasa reprezentująca opinie w programie i bazie 
    /// </summary>
    public partial class Opinion
    {
         public Opinion()
        {
            
        }
        [Key]
        public int Id { get; set; }

        public int CeneoOpinionId { get; set; }
        public string ProsList { get; set; }
        public string ConsList { get; set; }
        public string Author { get; set; }
        public string OpinionSummary { get; set; }
        public float StarsAvrage { get; set; }
        public DateTime OpinionDateTime { get; set; }
        public string ProductRecommended { get; set; }
        public int OpinionUsefullnesCount { get; set; }
        public virtual CeneoProduct Product { get; set; }
        public string src { get; set; }
    }
}
