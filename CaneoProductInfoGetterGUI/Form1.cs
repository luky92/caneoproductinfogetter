﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlTypes;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CeneneoProductInfoGetterDLL;
using HtmlAgilityPack;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using Microsoft.SqlServer.Server;

namespace CaneoProductInfoGetterGUI
{
   /// <summary>
   /// Główna klasa aplikacji
   /// </summary>
    public partial class Form1 : Form
    {
        CeneoProduct Product = new CeneoProduct();
        List<Opinion> Opinions = new List<Opinion>();
        /// <summary>
        /// inicjalizuje klase i ustawia początkowy stan elementów formatki
        /// </summary>
        public Form1()
        {
            InitializeComponent();
            Transform.Enabled = false;
            button3.Enabled = false;
        }
        /// <summary>
        /// Urucchamia proces extarkcji danych odwołując się do zewnętrznej aplikacji w technologii autoit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Exact_Click(object sender, EventArgs e) // exact 
        {
            /* CeneoProduct Product= new CeneoProduct() {Make = "Test", Opinions = new List<Opinion>()};
             using (var ctx = new CeneoContext())
             {
                  ctx.Products.Add(Product);
                 ctx.SaveChanges();
             }
             */
              int n;
            bool isNumeric = int.TryParse(textBox1.Text, out n);
            if (!isNumeric)
            {
                MessageBox.Show("ID nie jest liczbą");
              
                return;
            }

            if (textBox1.Text.Length == 0)
            {
                MessageBox.Show("Prosze podać ID");
                return;
            }
            button4.Enabled = false;
            Process compiler = new Process();
            compiler.StartInfo.FileName = "Extractor.exe";
            //    compiler.StartInfo.Arguments = "/r:System.dll /out:sample.exe stdstr.cs";
            compiler.StartInfo.Arguments = textBox1.Text;
            compiler.StartInfo.UseShellExecute = false;
            compiler.StartInfo.RedirectStandardOutput = true;
            //compiler.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            //compiler.EnableRaisingEvents = true;
            compiler.StartInfo.CreateNoWindow = true;
           // compiler.OutputDataReceived += new DataReceivedEventHandler(StdOut);
            compiler.Start();

            richTextBox1.Text = richTextBox1.Text + compiler.StandardOutput.ReadToEnd();
            richTextBox1.SelectionStart = richTextBox1.Text.Length - 1;
            richTextBox1.ScrollToCaret();
            compiler.WaitForExit();
            Transform.Enabled = true;
            Exact.Enabled = false;

        }
        /// <summary>
        /// wykonuje proces transformacji z danych otrzymanych od extraktora
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Transform_Click(object sender, EventArgs e) //transform
        {
            richTextBox1.Text += "Trasform start";
            Product.CeneoProductId = Convert.ToInt32(textBox1.Text);
            
            DirectoryInfo dinfo = new DirectoryInfo(Application.StartupPath);
            FileInfo[] Files = dinfo.GetFiles("*.txt");
            foreach (FileInfo file in Files)
            {
                if (file.Name.Contains("prod"))
                {
                    richTextBox1.Text += "ceneoo";
                    string s=File.ReadAllText(file.Name);
                    string[] words = s.Split('\n');
                    Product.ProductType = words[0];
                    Product.Make = words[1];
                    foreach (var VARIABLE in words)
                    {
                        if (VARIABLE.Contains("(")) Product.Model = VARIABLE;
                    }
                    Product.CeneoProductId = Convert.ToInt32(textBox1.Text);
                }
                try
                {
                    HtmlWeb web = new HtmlWeb();
                    HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                    doc.LoadHtml(File.ReadAllText(file.Name));
                    if (file.Name.Contains("op")) { 
                    HtmlNodeCollection nodeCol =
                        doc.DocumentNode.SelectNodes("//li[@class=\"product-review js_product-review\"]");
                        foreach (HtmlNode htmlnode in nodeCol)
                        {
                            Opinion tempOpinion = new Opinion();
                            HtmlNodeCollection ncol =
                                htmlnode.SelectNodes(".//div[@class=\"show-review-content content-wide\"]");
                            HtmlNodeCollection ntCollection = ncol[0].SelectNodes(".//div[@class=\"content-wide-col\"]");
                            HtmlNode hnoo = ntCollection[0].SelectSingleNode(".//span[@class=\"review-score-count\"]");
                            tempOpinion.StarsAvrage =
                                float.Parse(hnoo.InnerText.Substring(0, hnoo.InnerText.IndexOf("/")));
                            HtmlNode htn =
                                ntCollection[0].SelectSingleNode(".//div[contains(@id,\"product-review-comments\")]");
                            string copid = htn.Id.Substring(htn.Id.LastIndexOf("-") + 1);
                            tempOpinion.CeneoOpinionId = Convert.ToInt32(copid);
                            HtmlNodeCollection nc = htmlnode.SelectNodes(".//header[@class=\"product-review-header\"]");
                            HtmlNode hn = nc[0];
                            HtmlNodeCollection nc2 = hn.SelectNodes(".//div[@class=\"reviewer-cell\"]");
                            HtmlNode hmm = nc2[0].SelectSingleNode(".//div[@class=\"product-reviewer\"]");
                            //MessageBox.Show(hmm.InnerText);
                            tempOpinion.Author = hmm.InnerText;
                            HtmlNode rb = htmlnode.SelectSingleNode(".//p[@class=\"product-review-body\"]");
                            tempOpinion.OpinionSummary = rb.InnerHtml;
                            rb = htmlnode.SelectSingleNode(".//span[@class=\"pros-cell\"]");
                          //  tempOpinion.ProsList = new List<string>();
                            tempOpinion.ProsList=rb.InnerText.Replace("ZALETY","");
                            rb = htmlnode.SelectSingleNode(".//span[@class=\"cons-cell\"]");
                          //tempOpinion.ConsList = new List<string>();
                            tempOpinion.ConsList=rb.InnerText.Replace("WADY","");
                            rb = htmlnode.SelectSingleNode(".//span[@class=\"review-time\"]");
                            // review - time
                            string data = rb.InnerHtml;
                            data = data.Substring(data.IndexOf("\"") + 1, 19);
                            tempOpinion.OpinionDateTime = Convert.ToDateTime(data);
                            // product-recommended
                            rb = htmlnode.SelectSingleNode(".//em[@class=\"product-recommended\"]");
                            tempOpinion.ProductRecommended = rb.InnerText;
                            int a = 12;
                            tempOpinion.src = "ceneo";
                            Opinions.Add(tempOpinion);
                        }
                    }
                    if (file.Name.Contains("mor"))
                    {
                        richTextBox1.Text += "morele";
                        List<int> myids = new List<int>();
                        List<string> Authors= new List<string>();
                        List<string> opsList = new List<string>();
                        List<string> plusy = new List<string>();
                        List<string> munusy = new List<string>();
                        List<string> datesList= new List<string>();
                        HtmlNodeCollection html= doc.DocumentNode.SelectNodes(".//ul[@class=\"left-comment review\"]");
                        foreach (var V in html)
                        {
                            opsList.Add(V.SelectSingleNode(".//li[@class=\"rcomment\"]").InnerText);
                            string plus=V.SelectSingleNode(".//li[@class=\"good\"]").InnerText;
                            string temp = plus.Substring(plus.IndexOf("Plusy"));
                            temp = temp.Replace("Plusy:", "");
                            plusy.Add(temp);
                           // plusy.Add(plus.Substring(plus.IndexOf("Plusy")));
                            string minus= V.SelectSingleNode(".//li[@class=\"bad\"]").InnerText;
                            temp = minus.Substring(minus.IndexOf("Minusy"));
                            temp=temp.Replace("Minusy:", "");
                            munusy.Add(temp);
                            // munusy.Add(minus.Substring(minus.IndexOf("Minusy")));
                        }
                        html = doc.DocumentNode.SelectNodes(".//ul[@class=\"right-comment review\"]");
                        foreach (var V in html)
                        {
                            datesList.Add(V.SelectSingleNode(".//li[@class=\"date\"]").InnerText);
                            string aut = V.SelectSingleNode(".//li[@class=\"author\"]").InnerText;
                            aut = aut.Substring(aut.IndexOf("Autor:")).Replace("Autor:","");
                            Authors.Add(aut);
                        }
                        //int a = 123;

                        for (int i = 0; i < opsList.Count; i++)
                        {
                            Opinion opinion= new Opinion();
                            opinion.OpinionSummary = opsList[i];
                            opinion.Author = Authors[i];
                           // opinion.ProsList= new List<string>();
                            opinion.ProsList=plusy[i];
                          //  opinion.ConsList=new List<string>();
                            opinion.ConsList=munusy[i];
                            opinion.OpinionDateTime = Convert.ToDateTime(datesList[i]);
                            opinion.src = "morele";
                            Opinions.Add(opinion);

                        }
                    }
                    if (file.Name.Contains("comp"))
                    {
                        richTextBox1.Text += "komputronik";
                        List<string> opsList = new List<string>();
                        List<DateTime> datesList = new List<DateTime>();
                        List<float> markList= new List<float>();
                        HtmlNodeCollection html = doc.DocumentNode.SelectNodes(".//div[@class=\"opinionContent\"]");
                        foreach (var V in html)
                        {
                            opsList.Add(V.InnerText);

                        }
                         html = doc.DocumentNode.SelectNodes(".//div[@class=\"addDate\"]");
                        foreach (var V in html)
                        {
                            datesList.Add(Convert.ToDateTime(V.InnerText));


                        }
                        html = doc.DocumentNode.SelectNodes(".//div[@class=\"summary comment\"]");
                        foreach (var V in html)
                        {
                            string mark = V.InnerText.Substring(0, V.InnerText.IndexOf("/"));
                            markList.Add(float.Parse(mark));


                        }
                        for (int i = 0; i < opsList.Count; i++)
                        {
                            Opinion opinion = new Opinion();
                            opinion.OpinionSummary = opsList[i];
                            opinion.Author = "komputronik";
                            opinion.ProsList = "";

                            opinion.ConsList = "";
                           opinion.OpinionDateTime = Convert.ToDateTime(datesList[i]);
                            opinion.src = "komputronik";
                            Opinions.Add(opinion);

                        }
                    }

                }
                catch
                {

                   // richTextBox1.Text += "Wyjątek";
                }


               
            }
            foreach (var file in Files)
            {
                File.Delete(file.Name);

            }
            Transform.Enabled = false;
            button3.Enabled = true;
        }
        /// <summary>
        /// Zapisuje ztransormowanie dane do bazy
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Load_Click(object sender, EventArgs e) //load
        {
          
            using (CeneoContext ctxCeneoContext= new CeneoContext())
            {
                if (ctxCeneoContext.Products.Any(o => o.CeneoProductId == Product.CeneoProductId))
                {
                    var qu = from VAR in ctxCeneoContext.Products
                        where VAR.CeneoProductId == Product.CeneoProductId
                        select VAR;
                    Product = qu.ToList()[0];
                    foreach (var VARIABLE in Opinions)
                    {
                        if (VARIABLE.CeneoOpinionId != 0 && !(from VAR in ctxCeneoContext.Opinions where VAR.CeneoOpinionId==VARIABLE.CeneoOpinionId select VAR).Any()
                        )
                        {
                            VARIABLE.Product = Product;
                            ctxCeneoContext.Opinions.Add(VARIABLE);
                            ctxCeneoContext.SaveChanges();
                        }
                            if (VARIABLE.CeneoOpinionId != 0 && ctxCeneoContext.Opinions.Any(o => o.CeneoOpinionId == VARIABLE.CeneoOpinionId))

                        {
                            var result = ctxCeneoContext.Opinions.SingleOrDefault(b => b.CeneoOpinionId == VARIABLE.CeneoOpinionId);
                            if (result != null)
                            {
                                result.ConsList = VARIABLE.ConsList;
                                result.OpinionUsefullnesCount = VARIABLE.OpinionUsefullnesCount;
                                result.OpinionDateTime = VARIABLE.OpinionDateTime;
                                result.StarsAvrage = VARIABLE.StarsAvrage;
                                result.OpinionSummary = VARIABLE.OpinionSummary;
                                result.ProductRecommended = VARIABLE.ProductRecommended;
                                result.ProsList = VARIABLE.ProsList;


                                ctxCeneoContext.SaveChanges();
                            }
                            
                            continue;

                        }
                        if (VARIABLE.Author == "komputronik" && ctxCeneoContext.Opinions.Any(o => o.Author == "komputronik" && o.OpinionDateTime == VARIABLE.OpinionDateTime && o.OpinionSummary == VARIABLE.OpinionSummary))
                        {
                            var result = ctxCeneoContext.Opinions.SingleOrDefault(o => o.Author == "komputronik" && o.OpinionDateTime == VARIABLE.OpinionDateTime && o.OpinionSummary == VARIABLE.OpinionSummary);
                            if (result != null)
                            {
                                result.ConsList = VARIABLE.ConsList;
                                result.OpinionUsefullnesCount = VARIABLE.OpinionUsefullnesCount;
                                result.OpinionDateTime = VARIABLE.OpinionDateTime;
                                result.StarsAvrage = VARIABLE.StarsAvrage;
                                result.OpinionSummary = VARIABLE.OpinionSummary;
                                result.ProductRecommended = VARIABLE.ProductRecommended;
                                result.ProsList = VARIABLE.ProsList;


                                ctxCeneoContext.SaveChanges();
                            }
                            continue;

                        }
                        if (VARIABLE.CeneoOpinionId == 0 && VARIABLE.Author != "komputronik" && ctxCeneoContext.Opinions.Any(o => o.Author != "komputronik" && o.CeneoOpinionId == 0 && o.Author == VARIABLE.Author && o.OpinionDateTime == VARIABLE.OpinionDateTime && o.OpinionSummary == VARIABLE.OpinionSummary))
                        {
                            var result =
                                ctxCeneoContext.Opinions.SingleOrDefault(
                                    o =>
                                        o.CeneoOpinionId == 0 && o.Author == VARIABLE.Author &&
                                        o.OpinionDateTime == VARIABLE.OpinionDateTime &&
                                        o.OpinionSummary == VARIABLE.OpinionSummary);
                            if (result != null)
                            {
                                result.ConsList = VARIABLE.ConsList;
                                result.OpinionUsefullnesCount = VARIABLE.OpinionUsefullnesCount;
                                result.OpinionDateTime = VARIABLE.OpinionDateTime;
                                result.StarsAvrage = VARIABLE.StarsAvrage;
                                result.OpinionSummary = VARIABLE.OpinionSummary;
                                result.ProductRecommended = VARIABLE.ProductRecommended;
                                result.ProsList = VARIABLE.ProsList;


                                ctxCeneoContext.SaveChanges();
                            }
                            continue;
                        }
                        VARIABLE.Product = Product;
                        ctxCeneoContext.Opinions.Add(VARIABLE);
                    }

                    System.Diagnostics.Process.Start(Application.ExecutablePath); // to start new instance of application
                    this.Close(); //to turn off current app
                    return;
                    
                }
                Product.Opinions = Opinions;
                ctxCeneoContext.Products.Add(Product);
    
                ctxCeneoContext.SaveChanges();

            }
            // System.Threading.Thread.Sleep(50000);
            // Form1_Load(null,null);
            System.Diagnostics.Process.Start(Application.ExecutablePath); // to start new instance of application
            this.Close(); //to turn off current app
        }
        /// <summary>
        /// Wykonuje pełny proces ETL odwołując sie do poprzednich funkcji 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ETL_Click(object sender, EventArgs e) //all
        {
            int n;
            bool isNumeric = int.TryParse(textBox1.Text, out n);
            if (!isNumeric)
            {
                MessageBox.Show("ID nie jest liczbą");
                return;
            }

            if (textBox1.Text.Length == 0)
            {
                MessageBox.Show("Prosze podać ID");
                return;
            }
            Exact_Click(null,null);
            Transform_Click(null,null);
            Load_Click(null,null);
        }
        /// <summary>
        /// Usuwa wszystkie dane z bazy
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Nuke_Click(object sender, EventArgs e) //nuke
        {
            using (CeneoContext ctxCeneoContext = new CeneoContext())
            {
                //ctxCeneoContext.Database.ExecuteSqlCommand("TRUNCATE TABLE Products");
                var rows = from o in ctxCeneoContext.Opinions
                           select o;
                foreach (var row in rows)
                {
                    ctxCeneoContext.Opinions.Remove(row);
                }
                ctxCeneoContext.SaveChanges();
                var rows2 = from o in ctxCeneoContext.Products 
                           select o;
                foreach (var row in rows2)
                {
                    ctxCeneoContext.Products.Remove(row);
                }
                ctxCeneoContext.SaveChanges();
                System.Diagnostics.Process.Start(Application.ExecutablePath); // to start new instance of application
                this.Close(); //to turn off current app
            }
        }
        /// <summary>
        /// Obsługuje wyświetlanie listy opini z bazy 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ComboBox cbx = (ComboBox) sender;
                CeneoProduct p = (CeneoProduct) cbx.SelectedValue;
                int id = p.Id;
                using (CeneoContext ctCeneoContext = new CeneoContext())
                {
                    var qr = from o in ctCeneoContext.Opinions
                        where o.Product.Id == id
                        select
                            new
                            {
                                OpinionId = o.Id,
                                Plusy = o.ProsList.ToString(),
                                Minusy = o.ConsList.ToString(),
                                Autor = o.Author,
                                opinia = o.OpinionSummary,
                                Srednia = o.StarsAvrage,
                                Data = o.OpinionDateTime.ToString(),
                                Polecono = o.ProductRecommended,
                                Użyteczna = o.OpinionUsefullnesCount,
                                Żródło = o.src
                                
                            };
                    dataGridView2.DataSource = qr.ToList();



                }
            }
            catch
            {
                ComboBox cbx = (ComboBox) sender;
                int id = (int) cbx.SelectedValue;
                using (CeneoContext ctCeneoContext = new CeneoContext())
                {
                    var q = from o in ctCeneoContext.Opinions
                        where o.Product.Id == id
                        select
                            new
                            {
                                OpinionId = o.Id,
                                Plusy = o.ProsList.ToString(),
                                Minusy = o.ConsList.ToString(),
                                Autor = o.Author,
                                opinia = o.OpinionSummary,
                                Srednia = o.StarsAvrage,
                                Data = o.OpinionDateTime.ToString(),
                                Polecono = o.ProductRecommended,
                                Użyteczna = o.OpinionUsefullnesCount,
                                Żródło = o.src
                          
                            };
                    dataGridView2.DataSource = null;
                    dataGridView2.DataSource = q.ToList();
                    dataGridView2.Update();


                }
            }
        }
        /// <summary>
        /// Wypelmia comobobox wyboru produktów
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Load(object sender, EventArgs e)
        {
            using (CeneoContext ctxCeneoContex= new CeneoContext())
            {
                comboBox1.DataSource = ctxCeneoContex.Products.ToList();
                comboBox1.ValueMember = "id";
                comboBox1.DisplayMember = "CeneoProductId";
                this.FormBorderStyle = FormBorderStyle.FixedSingle;


            }
        }
        /// <summary>
        /// eksportuje dane do pliku csv
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Export_Click(object sender, EventArgs e)
        {
            using (CeneoContext ctxCeneoContext = new CeneoContext())
            {
                using (StreamWriter ofileWriter = new StreamWriter("export.csv"))
                {
                    ofileWriter.WriteLine("OpinionId,CeneoOpinionId,src,CeneoProductId,ProsList,ConsList,Author,OpSummary,starsAvg,OpDT,ProductRecomended,OpinionUsefullnessCount");
                }
               

                List<Opinion> opinions = ctxCeneoContext.Opinions.ToList();
                using (StreamWriter ofileWriter = new StreamWriter("export.csv", true))
                {
                    foreach (var op in opinions)
                    {
                        string line = "";
                        line += op.Id.ToString() + ",";
                        line += op.CeneoOpinionId.ToString() + ",";
                        line += op.src + ",";
                        line += op.Product.CeneoProductId.ToString() + ",";
                        string pl = op.ProsList.Replace("\n", " ");
                        line += "\"" + pl + "\",";
                        string cl = op.ConsList.Replace("\n", " ");
                        line += "\"" + cl + "\",";
                        line += op.Author.Replace("\n", "")+",";
                        line +="\""+ op.OpinionSummary.Replace("\n", " ")+"\",";
                        line += op.StarsAvrage.ToString().Replace("\n", "") + ",";
                        line += op.OpinionDateTime.ToString() + ",";
                        line += op.ProductRecommended + ",";
                        line += op.OpinionUsefullnesCount;
                        string cleanedString = System.Text.RegularExpressions.Regex.Replace(line, @"\s+", " ");
                        ofileWriter.WriteLine(cleanedString);


                    }
                }
            }
        }
    }
}

    




